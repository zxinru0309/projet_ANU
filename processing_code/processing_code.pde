import processing.sound.*;


import meter.*;
import ddf.minim.*;
import processing.serial.*;

Minim minim;
AudioPlayer player;
Serial port; 

Meter m, m2;

void setup() {

  size(950, 400); // Size of the window (width, height)
  background(0, 0, 0); // Background color of window (R,G,B)
  minim = new Minim(this);
  player = minim.loadFile("bomb.mp3", 2048);
  String arduinoPort = Serial.list()[3];
  print (arduinoPort);
  port = new Serial(this, arduinoPort, 9600);

  m = new Meter(this, 25, 80);  // here 25, 10 are x and y coordinates of meter's upper left corner

  m.setTitleFontSize(20);
  m.setTitleFontName("Arial bold");
  m.setTitle("Temperature (C)");

  // Change meter scale values
  String[] scaleLabels = {"0", "10", "20", "30", "40", "50", "60", "70", "80"};
  m.setScaleLabels(scaleLabels);
  m.setScaleFontSize(18);
  m.setScaleFontName("Times new roman bold");
  m.setScaleFontColor(color(200, 30, 70));


  m.setDisplayDigitalMeterValue(true);


  m.setArcColor(color(141, 113, 178));
  m.setArcThickness(15);

  m.setMaxScaleValue(80);

  m.setMinInputSignal(0);
  m.setMaxInputSignal(80);

  m.setNeedleThickness(3);

  int mx = m.getMeterX(); // x coordinate of m
  int my = m.getMeterY(); // y coordinate of m
  int mw = m.getMeterWidth();

  m2 = new Meter(this, mx + mw + 20, my);

  m2.setTitleFontSize(20);
  m2.setTitleFontName("Arial bold");
  m2.setTitle("Humidity (%)");

  String[] scaleLabels2 = {"0", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100"};
  m2.setScaleLabels(scaleLabels2);
  m2.setScaleFontSize(18);
  m2.setScaleFontName("Times new roman bold");
  m2.setScaleFontColor(color(200, 30, 70));


  m2.setDisplayDigitalMeterValue(true);


  m2.setArcColor(color(141, 113, 178));
  m2.setArcThickness(15);

  m2.setMaxScaleValue(100);

  m2.setMinInputSignal(0);
  m2.setMaxInputSignal(100);

  m2.setNeedleThickness(3);
  m.updateMeter(0); 
  m2.updateMeter(0);
}

void draw() {
  textSize(30);
  fill(0, 255, 0); // Font color , (r,g,b)
  text("Arduino ", 250, 40); // ("text", x, y)

  if (port.available() > 0) {
    String val = port.readStringUntil('\n');
    if (val!=null)
    {
      print(val);





      try {  
        // First we need to separate temperature and humidity values
        String[] list = split(val, ','); // splits value separated by ','
        float temp = float(list[0]); // first value is Temperature
        float hum = float(list[1]);  // second value is Humidity

        m.updateMeter(int(temp)); 
        m2.updateMeter(int(hum));

        println("Temperature: " + temp + " C  " + "Humidity: " + hum+ "%");
        if (temp>23 && hum<90) 
        {
         //player = minim.loadFile("01.wav", 2048);
          player.play();
        }
      } 
      catch (Exception e) {
      }
    }
  }
}