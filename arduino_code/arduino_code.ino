#include <DHT.h>
#include <Servo.h>
#define DHTPIN 4   //dht11
#define SERVOPIN 9  // 水泵
#define DHTTYPE DHT11

Servo ms;
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  ms.attach(SERVOPIN);
  dht.begin();
  ms.write(90);
  Serial.println();
}

void loop() {
  delay(2000);
  int h = dht.readHumidity();
  int t = dht.readTemperature();
  Serial.print(t);
  Serial.print(",");
  Serial.print(h);
  Serial.println();
  if ((t > 23) && (h < 90)){
     ms.write(180);
     delay(10000);
  }
}
